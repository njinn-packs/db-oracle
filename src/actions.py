import subprocess
import tempfile

import cx_Oracle

MAX_RESULTS_NUM = 10_000

AUTHENTICATION_MODES_MAP = {
    "Default": cx_Oracle.DEFAULT_AUTH,
    "SYSDBA": cx_Oracle.SYSDBA,
    "SYSASM": cx_Oracle.SYSASM,
    "SYSBKP": cx_Oracle.SYSBKP,
    "SYSDGD": cx_Oracle.SYSDGD,
    "SYSKMT": cx_Oracle.SYSKMT,
    "SYSOPER": cx_Oracle.SYSOPER,
    "SYSRAC": cx_Oracle.SYSRAC,
}


class OracleConnectionBase:
    oracle_connection = {}


class CxOracleConnection(OracleConnectionBase):
    cnxn = None
    cursor = None

    def connect(self):
        con_options = {}
        custom_connection = self.oracle_connection.get("custom_connection")
        if custom_connection:
            con_options["user"] = custom_connection
            print("Establishing connection using custom connection string.")
        else:
            con_options["user"] = self.oracle_connection.get("user")
            con_options["password"] = self.oracle_connection.get("password")
            con_options["dsn"] = self.get_dsn()

            con_options_display = {**con_options}

            mode = self.oracle_connection.get("mode", "Default")
            con_options_display["mode"] = mode
            con_options["mode"] = AUTHENTICATION_MODES_MAP.get(mode)

            print("Establishing connection using connection parameters:")
            print(
                {k: v for k, v in con_options_display.items() if k not in ["password"]},
                end="\n\n",
            )

        try:
            self.cnxn = cx_Oracle.connect(**con_options)
        except cx_Oracle.DatabaseError as e:
            raise Exception("Error while connecting: %s" % e)
        self.cursor = self.cnxn.cursor()

    def disconnect(self):
        if self.cnxn:
            self.cnxn.close()
            print("\nConnection closed")

    def get_dsn(self):
        host = self.oracle_connection.get("host")
        if not host:
            raise Exception("No database host provided")
        port = self.oracle_connection.get("port")
        service = self.oracle_connection.get("service")
        try:
            return cx_Oracle.makedsn(host, port, service_name=service)
        except cx_Oracle.DatabaseError as e:
            raise Exception("Error while building dsn: %s" % e)


class SQLPlusOracleConnection(OracleConnectionBase):
    def get_connection_string(self):
        connection_string = ""
        custom_connection = self.oracle_connection.get("custom_connection")
        if custom_connection:
            connection_string = custom_connection
            print("Establishing connection using custom connection string.")
        else:
            connection_string = self._get_connection_string()
            print("Establishing connection using connection string:")
            print(self._get_connection_string(hide_password=True), end="\n\n")

        return connection_string

    def _get_connection_string(self, hide_password=False):
        conn_string = "{}/{}@{}:{}/{}".format(
            self.oracle_connection.get("user"),
            self.oracle_connection.get("password") if not hide_password else "******",
            self.oracle_connection.get("host"),
            self.oracle_connection.get("port"),
            self.oracle_connection.get("service"),
        )
        mode = self.oracle_connection.get("mode")
        if mode is not None and mode != "Default":
            conn_string += f" as {mode}"

        return conn_string


class RunQuery(CxOracleConnection):
    query = ""
    auto_commit = True

    def run(self):
        if not self.query:
            raise Exception("No query defined to execute")

        results_sets = []
        self.connect()
        try:
            query_parts = [q.strip("\n") for q in self.query.split(";") if q.strip()]
            for qry in query_parts:
                print("Executing query:\n", qry)
                try:
                    self.cursor.execute(qry)
                except cx_Oracle.DatabaseError as e:
                    raise Exception("Error while executing a query: %s" % e)

                if self.cursor.description:
                    self.make_output_a_dict()

                try:
                    result = self.cursor.fetchall()
                except cx_Oracle.InterfaceError:
                    print("Query didn't return a result")
                    result = []

                if result:
                    result_length = self.cursor.rowcount
                    print(f"{result_length} row(s) retrieved")
                    if result_length > MAX_RESULTS_NUM:
                        print(f"Results truncated to first {MAX_RESULTS_NUM} elements")
                    rows = result[:MAX_RESULTS_NUM]
                    results_sets.append(rows)

            if self.auto_commit:
                self.cnxn.commit()
        finally:
            self.disconnect()

        action_result = {}
        results_sets_length = len(results_sets)
        if results_sets_length == 1:
            action_result["rows"] = results_sets[0]
        elif results_sets_length > 1:
            for idx, set in enumerate(results_sets):
                action_result[f"rows_{idx}"] = set
        return action_result

    def make_output_a_dict(self):
        columns = [col[0] for col in self.cursor.description]
        self.cursor.rowfactory = lambda *args: dict(zip(columns, args))


class RunPLSQLScript(SQLPlusOracleConnection):
    # Action inputs
    script = ""
    arguments = ""

    # Internal attributes
    script_file = None
    script_path = ""

    def run(self):
        if not self.script:
            raise Exception("No script to execute")

        self.prepare_script_file()

        cmd = self.get_run_command()
        try:
            proc = subprocess.Popen(
                cmd,
                universal_newlines=True,
                text=True,
                stderr=subprocess.STDOUT,
                stdout=subprocess.PIPE,
                shell=True,
                start_new_session=True,
                bufsize=1,
                encoding="utf_8",
                errors="replace",
            )

            output_lines = []
            read_stdout(proc, output_lines)

            if proc.returncode != 0:
                handle_error(output_lines)
        finally:
            self.clean_up()

        return None

    def get_run_command(self) -> str:
        # Exit SQL*Plus after script execution
        # Solution found here: https://serverfault.com/a/87038
        exit_after_script_execution = [
            "echo",
            "exit",
            "|",
        ]

        cmd = [
            *exit_after_script_execution,
            "sqlplus",
            "-S",
            "-L",
            self.get_connection_string(),
            f"@{self.script_path}",
        ]
        if self.arguments:
            for arg in self.arguments.split():
                cmd.append(arg)

        cmd_str = " ".join(cmd)
        return cmd_str

    def prepare_script_file(self):
        self.script_file = tempfile.NamedTemporaryFile(suffix=".sql", delete=False)
        self.script_path = self.script_file.name
        script = self.script.replace("\r", "")
        with open(self.script_path, "w", encoding="utf_8") as f:
            # Prepend to the script so it'll return with proper exit code
            f.write("WHENEVER SQLERROR EXIT SQL.SQLCODE ROLLBACK\n")
            f.write("WHENEVER OSERROR EXIT FAILURE ROLLBACK\n")
            f.write(script)

    def clean_up(self):
        if self.script_file is not None:
            self.script_file.close()


def read_stdout(proc, lines):
    """ Reads all remaining stdout """
    buffer = []
    if proc and proc.stdout:
        while True:
            line = proc.stdout.readline()
            if not line and proc.poll() is not None:
                return
            # under windows, there is a timing issue with poll() that can cause us
            # to append many empty lines at the end
            # We avoid this without truncating empty lines in the middle of the output by only
            # adding to lines when current line is nonempty.
            buffer.append(line)
            if line.strip():
                [print(l.strip()) for l in buffer]
                lines.extend(buffer)
                buffer = []
    return None


def handle_error(output_lines):
    """Truncate output if needed and raise an Exception"""
    LAST_N_ELEMENTS = 6
    error_lines = output_lines[-LAST_N_ELEMENTS:]
    # If list of max N last elements has length N
    # it means output has been truncated
    if len(error_lines) == LAST_N_ELEMENTS:
        error_lines.insert(0, "...(output omitted here)...\n")

    error_msg = "".join(error_lines)

    if "error while loading shared libraries: libsqlplus.so" in error_msg:
        error_msg += (
            "\nYou must configure SQL*Plus on the worker in order to run this action."
        )
        error_msg += "\nSee https://www.oracle.com/database/technologies/instant-client/downloads.html for help"
    raise Exception(error_msg)
